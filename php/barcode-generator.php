<?php
  class BarcodeGenerator {
    private $characters = [];
    private $length = null;
    private $lastBarcode = null;
    private $indexes = [];
    
    private const DEFAULT_LENGTH = 32;
    public const MIN_LENGTH = 8;
    public const MAX_LENGTH = 64;
    
    private function normilize_characters($characters = []) {
      if (!isset($characters) || !is_array($characters)) {
        $characters = [];
      }
      
      $characters = array_filter(
        $characters,
        function($element) {
          return is_string($element) && mb_strlen($element) === 1;
        }
      );
      
      $characters = array_unique($characters);
      return $characters;
    }
    
    private function normilize_length($length) {
      if (!isset($length)) {
        return self::DEFAULT_LENGTH;
      }
      
      if ($length < self::MIN_LENGTH || $length > self::MAX_LENGTH) {
        throw new Exception("[BarcodeService] Configuration for \"length\" is out of range. Define length between " . self::MIN_LENGTH ." and " . self::MAX_LENGTH . ".");
        return;
      }
      
      return $length;
    }
    
    private function initialize_indexes() {
      if (!isset($this->lastBarcode)) {
        for($i=0; $i<$this->length; $i++) {
          $this->indexes[$i] = 0;
        }
        $this->indexes[count($this->indexes)-1] = -1;
      } else {
        for($position = $this->length-1; $position >= 0; $position--) {
          $index = array_search($this->lastBarcode[$position], $this->characters);
          $this->indexes[$position] = $index;
        }
      }
      
    }
    
    private function normilize_barcode($length, $barcode, $characters) {
      if (isset($barcode)) {
        if (mb_strlen($barcode) !== $length) {
          throw new Exception("[BarcodeService] Barcode length and configuration for \"length\" are not the same.");
          return;
        }
        
        for($i=0; $i<mb_strlen($barcode); $i++) {
          if (!in_array($barcode[$i], $characters)) {
            throw new Exception("[BarcodeService] Barcode contains invalid characters.");
            return;
          }
        }
      }
      
      return $barcode;
    }
    
    private function generate($indexes) {
      $barcode = [];
      
      $maxIndex = count($this->characters) - 1;
      
      for($position = $this->length - 1; $position >= 0; $position--) {
        $index = $indexes[$position] + 1;
        
        if ($index > $maxIndex) {
          for($i = $position; $i < $this->length; $i++) {
            $indexes[$i] = 0;
          }
          
          $indexes[$i-1] = $indexes[$i-1] + 1;
          return $this->generate($indexes);
        } else {
          $indexes[$position] = $index;
          
          for($i = $index - 1; $i >= 0; $i--) {
            $indexes[$position] = $index;
          }
          
          break;
        }
      }
      
      for($position = 0; $position < $this->length; $position++) {
        $barcode[$position] = $this->characters[
          $indexes[$position]
        ];
      }
      
      return join($barcode);
    }
    
    public function __construct($length = null, $lastBarcode = null, $characters = []) {
      $this->characters = $this->normilize_characters($characters);
      $this->length = $this->normilize_length($length);
      $this->lastBarcode = $this->normilize_barcode($this->length, $lastBarcode, $this->characters);
      $this->initialize_indexes();
    }
    
    public function next() {
      $barcode = $this->generate($this->indexes);
      $this->lastBarcode = $barcode;
      $this->initialize_indexes();
      return $barcode;
    }
  }
?>