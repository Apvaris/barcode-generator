# Avkada
Barcode Generator was developed for [PHP](https://www.php.net/releases/8_0_9.php) version 8.0.9.

## Examples
`$barcodeGenerator = new BarcodeGenerator($length, $lastBarcode, $characters);`
Where `$length` is optional parameter that defines preferable length of the barcode,
`$lastBarcode` is the last barcode, that was generated before (in case if you want to generate unique barcodes one-by-one and store the last one somewhere),
`$characters` is the list of allowed characters in the barcode.